$(function () {
    $(".birthDatePicker").persianDatepicker();
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function containNoDigit(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return true;
    }
    return false;
}

function showLegalContentFA() {
    document.getElementById("fatherCol").style.display = "none";
    document.getElementById("familyCol").style.display = "none";
    document.getElementById("customerCodeLabel").innerHTML = "شماره اقتصادی";
    document.getElementById("customerCodeInput").title = "شماره اقتصادی";
    document.getElementById("customerNameLabel").innerHTML = "نام شرکت";
    document.getElementById("customerNameInput").title = "نام شرکت";
    document.getElementById("customerBirthDateLabel").innerHTML = "تاریخ ثبت";
    document.getElementById("customerBirthDateInput").title = "تاریخ ثبت";
    document.getElementById("customerFamilyInput").value = null;
    document.getElementById("customerFatherInput").value = null;
    document.getElementById("customerFamilyInput").required = false;
    document.getElementById("customerFatherInput").required = false;
}

function showRealContentFA() {
    document.getElementById("fatherCol").style.display = "block";
    document.getElementById("familyCol").style.display = "block";
    document.getElementById("customerCodeLabel").innerHTML = "کد ملی";
    document.getElementById("customerCodeInput").title = "کد ملی";
    document.getElementById("customerNameLabel").innerHTML = "نام";
    document.getElementById("customerNameInput").title = "نام";
    document.getElementById("customerBirthDateLabel").innerHTML = "تاریخ تولد";
    document.getElementById("customerBirthDateInput").title = "تاریخ تولد";
    document.getElementById("customerFamilyInput").required = true;
    document.getElementById("customerFatherInput").required = true;
}

function showLegalContentEN() {
    document.getElementById("fatherCol").style.display = "none";
    document.getElementById("familyCol").style.display = "none";
    document.getElementById("customerCodeLabel").innerHTML = "Economic Code";
    document.getElementById("customerCodeInput").title = "Economic Code";
    document.getElementById("customerNameLabel").innerHTML = "Company Name";
    document.getElementById("customerNameInput").title = "Company Name";
    document.getElementById("customerBirthDateLabel").innerHTML = "Registration Date";
    document.getElementById("customerBirthDateInput").title = "Registration Date";
    document.getElementById("customerFamilyInput").value = null;
    document.getElementById("customerFatherInput").value = null;
    document.getElementById("customerFamilyInput").required = false;
    document.getElementById("customerFatherInput").required = false;
}

function showRealContentEN() {
    document.getElementById("fatherCol").style.display = "block";
    document.getElementById("familyCol").style.display = "block";
    document.getElementById("customerCodeLabel").innerHTML = "National Code";
    document.getElementById("customerCodeInput").title = "National Code";
    document.getElementById("customerNameLabel").innerHTML = "First Name";
    document.getElementById("customerNameInput").title = "First Name";
    document.getElementById("customerBirthDateLabel").innerHTML = "Birth Date";
    document.getElementById("customerBirthDateInput").title = "Birth Date";
    document.getElementById("customerFamilyInput").required = true;
    document.getElementById("customerFatherInput").required = true;
}
function invalidInputFA(inputText) {
    return "لطفا "+inputText+" را وارد نمایید";
}

function invalidInputEN(inputText) {
    return "Please enter "+inputText;
}

function invalidInputCustomerCodeFA(inputText) {
    return inputText+" باید 10 رقم باشد";
}

function invalidInputCustomerCodeEN(inputText) {
    return inputText+" must be 10 digit";
}

function invalidInputDateFA(inputText) {
    return "لطفا ' "+inputText+" ' را طبق فرمت مقابل وارد نمایید: روز/ماه/سال";
}

function invalidInputDateEN(inputText) {
    return "Please enter ' "+inputText+" ' in this format: Month/Day/Year";
}

function validateFindDateFA(){
    var startInput = document.getElementById("customerBirthDateInputFrom").value;
    var endInput = document.getElementById("customerBirthDateInputTo").value;
    var startDateValues = startInput.split("/");
    var endDateValues = endInput.split("/");
    var start_year = startDateValues[0];
    var start_month = startDateValues[1];
    var start_day = startDateValues[2];
    var end_year = endDateValues[0];
    var end_month = endDateValues[1];
    var end_day = endDateValues[2];
    if (start_year == end_year && start_month == end_month && start_day == end_day)
        return true;
    if (end_year > start_year)
        return true;
    if (end_year == start_year && end_month > start_month)
        return true;
    if (end_year == start_year && end_month == start_month && end_day > start_day)
        return true;
    alert("بازه ی تاریخ اشتباه است")
    return false;
}

function validateFindDateEN(){
    var startInput = document.getElementById("customerBirthDateInputFrom").value;
    var endInput = document.getElementById("customerBirthDateInputTo").value;
    var startDateValues = startInput.split("/");
    var endDateValues = endInput.split("/");
    var start_year = startDateValues[0];
    var start_month = startDateValues[1];
    var start_day = startDateValues[2];
    var end_year = endDateValues[0];
    var end_month = endDateValues[1];
    var end_day = endDateValues[2];
    if (start_year == end_year && start_month == end_month && start_day == end_day)
        return true;
    if (end_year > start_year)
        return true;
    if (end_year == start_year && end_month > start_month)
        return true;
    if (end_year == start_year && end_month == start_month && end_day > start_day)
        return true;
    alert("' Date From ' must be less than ' Date To '")
    return false;
}

function showLegalContentForFindFA() {
    document.getElementById("customerFamilyField").style.display = "none";
    document.getElementById("customerCodeLabel").innerHTML = "شماره اقتصادی";
    document.getElementById("customerCodeInput").title = "شماره اقتصادی";
    document.getElementById("customerNameLabel").innerHTML = "نام شرکت";
    document.getElementById("customerNameInput").title = "نام شرکت";
    document.getElementById("customerBirthDateLabelFrom").innerHTML = "تاریخ ثبت از";
    document.getElementById("customerBirthDateInputFrom").title = "تاریخ ثبت از";
    document.getElementById("customerBirthDateLabelTo").innerHTML = "تاریخ ثبت تا";
    document.getElementById("customerBirthDateInputTo").title = "تاریخ ثبت تا";
    document.getElementById("customerFamilyInput").value = null;
}

function showRealContentForFindFA() {
    document.getElementById("customerFamilyField").style.display = "block";
    document.getElementById("customerCodeLabel").innerHTML = "کد ملی";
    document.getElementById("customerCodeInput").title = "کد ملی";
    document.getElementById("customerNameLabel").innerHTML = "نام";
    document.getElementById("customerNameInput").title = "نام";
    document.getElementById("customerBirthDateLabelFrom").innerHTML = "تاریخ تولد از";
    document.getElementById("customerBirthDateInputFrom").title = "تاریخ تولد از";
    document.getElementById("customerBirthDateLabelTo").innerHTML = "تاریخ تولد تا";
    document.getElementById("customerBirthDateInputTo").title = "تاریخ تولد تا";
}

function showLegalContentForFindEN() {
    document.getElementById("customerFamilyField").style.display = "none";
    document.getElementById("customerCodeLabel").innerHTML = "Economic Code";
    document.getElementById("customerCodeInput").title = "Economic Code";
    document.getElementById("customerNameLabel").innerHTML = "Company Name";
    document.getElementById("customerNameInput").title = "Company Name";
    document.getElementById("customerBirthDateLabelFrom").innerHTML = "Registration Date From";
    document.getElementById("customerBirthDateInputFrom").title = "Registration Date From";
    document.getElementById("customerBirthDateLabelTo").innerHTML = "Registration Date To";
    document.getElementById("customerBirthDateInputTo").title = "Registration Date To";
    document.getElementById("customerFamilyInput").value = null;
}

function showRealContentForFindEN() {
    document.getElementById("customerFamilyField").style.display = "block";
    document.getElementById("customerCodeLabel").innerHTML = "National Code";
    document.getElementById("customerCodeInput").title = "National Code";
    document.getElementById("customerNameLabel").innerHTML = "First Name";
    document.getElementById("customerNameInput").title = "First Name";
    document.getElementById("customerBirthDateLabelFrom").innerHTML = "Birth Date From";
    document.getElementById("customerBirthDateInputFrom").title = "Birth Date From";
    document.getElementById("customerBirthDateLabelTo").innerHTML = "Birth Date To";
    document.getElementById("customerBirthDateInputFrom").title = "Birth Date From";
}
function changeLanguageToPersian() {
    document.languageForm.selectedLanguage.value = "Persian";
    document.languageForm.submit();
}
function changeLanguageToEnglish() {
    document.languageForm.selectedLanguage.value = "English";
    document.languageForm.submit();
}

