package utility;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class BankDatabaseUtility {
    private static Connection connection = null;

    public static Connection getConnection() {
        if (connection != null)
            return connection;
        else {
            try {
                Properties bankDatabaseProperties = new Properties();
                InputStream inputStream = BankDatabaseUtility.class.getClassLoader().getResourceAsStream("/bank-database.properties");
                bankDatabaseProperties.load(inputStream);
                String driver = bankDatabaseProperties.getProperty("driver");
                String url = bankDatabaseProperties.getProperty("url");
                String username = bankDatabaseProperties.getProperty("username");
                String password = bankDatabaseProperties.getProperty("password");
                Class.forName(driver);
                connection = DriverManager.getConnection(url, username, password);
            } catch (ClassNotFoundException | SQLException | IOException e) {
                e.printStackTrace();
            }
            return connection;
        }
    }
}
