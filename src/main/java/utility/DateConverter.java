package utility;

public class DateConverter {

    private static int[] gregorianToJalali(int gregorianYear, int gregorianMonth, int gregorianDay) {
        int[] g_d_m = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
        int gy2 = (gregorianMonth > 2) ? (gregorianYear + 1) : gregorianYear;
        int days = 355666 + (365 * gregorianYear) + ((int) ((gy2 + 3) / 4)) - ((int) ((gy2 + 99) / 100)) + ((int) ((gy2 + 399) / 400)) + gregorianDay + g_d_m[gregorianMonth - 1];
        int jy = -1595 + (33 * ((int) (days / 12053)));
        days %= 12053;
        jy += 4 * ((int) (days / 1461));
        days %= 1461;
        if (days > 365) {
            jy += (int) ((days - 1) / 365);
            days = (days - 1) % 365;
        }
        int jm = (days < 186) ? 1 + (int) (days / 31) : 7 + (int) ((days - 186) / 30);
        int jd = 1 + ((days < 186) ? (days % 31) : ((days - 186) % 30));
        int[] jalali = {jy, jm, jd};
        return jalali;
    }

    private static int[] jalaliToGregorian(int jalaliYear, int jalaliMonth, int jalaliDay) {
        jalaliYear += 1595;
        int days = -355668 + (365 * jalaliYear) + (((int) (jalaliYear / 33)) * 8) + ((int) (((jalaliYear % 33) + 3) / 4)) + jalaliDay + ((jalaliMonth < 7) ? (jalaliMonth - 1) * 31 : ((jalaliMonth - 7) * 30) + 186);
        int gy = 400 * ((int) (days / 146097));
        days %= 146097;
        if (days > 36524) {
            gy += 100 * ((int) (--days / 36524));
            days %= 36524;
            if (days >= 365)
                days++;
        }
        gy += 4 * ((int) (days / 1461));
        days %= 1461;
        if (days > 365) {
            gy += (int) ((days - 1) / 365);
            days = (days - 1) % 365;
        }
        int gd = days + 1;
        int[] sal_a = {0, 31, ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int gm;
        for (gm = 0; gm < 13 && gd > sal_a[gm]; gm++) gd -= sal_a[gm];
        int[] gregorian = {gy, gm, gd};
        return gregorian;
    }

    private static String dateToString(int[] dateValues, String delimiter) {
        int year = dateValues[0];
        int month = dateValues[1];
        int day = dateValues[2];
        return year + delimiter + month + delimiter + day;
    }

    private static int[] dateStringToDateValue(String dateString, String delimiter) {
        String[] values = dateString.split(delimiter);
        int year = Integer.parseInt(values[0]);
        int month = Integer.parseInt(values[1]);
        int day = Integer.parseInt(values[2]);
        int[] dateValues = {year, month, day};
        return dateValues;
    }

    public static String convertToJalali(String gregorianDate) {
        int[] gregorianValues = dateStringToDateValue(gregorianDate, "-");
        int[] jalaliValues = gregorianToJalali(gregorianValues[0], gregorianValues[1], gregorianValues[2]);
        return dateToString(jalaliValues, "/");
    }

    public static String convertToGregorian(String jalaliDate) {
        int[] jalaliValues = dateStringToDateValue(jalaliDate, "/");
        int[] gregorianValues = jalaliToGregorian(jalaliValues[0], jalaliValues[1], jalaliValues[2]);
        return dateToString(gregorianValues, "-");
    }
}
