package controller;

import model.dao.CustomerDAO;
import model.to.Customer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/show-customer")
public class ShowCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private CustomerDAO customerDAO;

    public void init() {
        customerDAO = new CustomerDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean customerTypeToFind = (boolean) request.getSession().getAttribute("customerTypeToFind");
        String customerIdToFind = (String) request.getSession().getAttribute("customerIdToFind");
        String customerCodeToFind = (String) request.getSession().getAttribute("customerCodeToFind");
        String customerNameToFind = (String) request.getSession().getAttribute("customerNameToFind");
        String customerFamilyToFind = (String) request.getSession().getAttribute("customerFamilyToFind");
        String birthDateFrom = (String) request.getSession().getAttribute("birthDateFrom");
        String birthDateTo = (String) request.getSession().getAttribute("birthDateTo");
        List<Customer> customers;
        try {
            customers = customerDAO.selectByParameters(customerTypeToFind, customerIdToFind, customerCodeToFind, customerNameToFind, customerFamilyToFind, birthDateFrom, birthDateTo);
            request.setAttribute("customers", customers);
            request.setAttribute("customerType", customerTypeToFind);
            RequestDispatcher dispatcher = request.getRequestDispatcher("customers.jsp");
            dispatcher.forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
