package controller;

import model.dao.CustomerDAO;
import model.to.Customer;
import utility.DateConverter;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLIntegrityConstraintViolationException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/update-customer")
public class UpdateCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private CustomerDAO customerDAO;

    public void init() {
        customerDAO = new CustomerDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String customerId = request.getParameter("id");
        String customerCode = request.getParameter("customerCode");
        String customerName = request.getParameter("customerName");
        String customerFamily = "";
        Date customerBirthDate;
        if (request.getParameter("customerFamily") != null)
            customerFamily = request.getParameter("customerFamily");
        String customerFather = "";
        if (request.getParameter("customerFather") != null)
            customerFather = request.getParameter("customerFather");
        if (request.getSession().getAttribute("selectedLanguage").equals("Persian"))
            customerBirthDate = Date.valueOf(DateConverter.convertToGregorian(request.getParameter("customerBirthDate")));
        else
            customerBirthDate = Date.valueOf(request.getParameter("customerBirthDate"));
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        customer.setCustomerCode(customerCode);
        customer.setCustomerName(customerName);
        customer.setCustomerFamily(customerFamily);
        customer.setCustomerFather(customerFather);
        customer.setCustomerBirthDate(customerBirthDate);
        try {
            customerDAO.update(customer);
            response.sendRedirect("show-customer");
        } catch (SQLIntegrityConstraintViolationException e) {
            if (request.getSession().getAttribute("selectedLanguage").equals("Persian"))
                request.setAttribute("errorMessage", "کد ملی/شماره اقتصادی مورد نظر در سیستم موجود می باشد. لطفا مقدار غیر تکراری وارد نمایید.");
            else
                request.setAttribute("errorMessage", "Another customer with this customer code already exists. Please enter a uniq code for your customer.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
