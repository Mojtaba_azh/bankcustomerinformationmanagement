package controller;

import model.dao.CustomerDAO;
import model.to.Customer;
import utility.DateConverter;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/insert-customer")
public class InsertCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private CustomerDAO customerDAO;

    public void init() {
        customerDAO = new CustomerDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        boolean customerType = true;
        if (request.getParameter("customerType").equals("legal"))
            customerType = false;
        Customer newCustomer = new Customer();
        newCustomer.setCustomerType(customerType);
        newCustomer.setCustomerCode(request.getParameter("customerCode"));
        newCustomer.setCustomerName(request.getParameter("customerName"));
        newCustomer.setCustomerFamily(request.getParameter("customerFamily"));
        newCustomer.setCustomerFather(request.getParameter("customerFather"));
        if (request.getSession().getAttribute("selectedLanguage").equals("Persian"))
            newCustomer.setCustomerBirthDate(Date.valueOf(DateConverter.convertToGregorian(request.getParameter("customerBirthDate"))));
        else
            newCustomer.setCustomerBirthDate(Date.valueOf((request.getParameter("customerBirthDate"))));
        try {
            customerDAO.insert(newCustomer);
            request.setAttribute("newCustomerId", customerDAO.selectLastCustomerId());
            RequestDispatcher dispatcher = request.getRequestDispatcher("success.jsp");
            dispatcher.forward(request, response);
        } catch (SQLIntegrityConstraintViolationException e) {
            if (request.getSession().getAttribute("selectedLanguage").equals("Persian"))
                request.setAttribute("errorMessage", "کد ملی/شماره اقتصادی مورد نظر در سیستم موجود می باشد. لطفا مقدار غیر تکراری وارد نمایید.");
            else
                request.setAttribute("errorMessage", "Another customer with this customer code already exists. Please enter a uniq code for your customer.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
            dispatcher.forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
