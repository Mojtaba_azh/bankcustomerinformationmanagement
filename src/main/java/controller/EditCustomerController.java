package controller;

import model.dao.CustomerDAO;
import model.to.Customer;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/edit-customer")
public class EditCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private CustomerDAO customerDAO;

    public void init() {
        customerDAO = new CustomerDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String customerId = request.getParameter("id");
            boolean customerType = true;
            if (request.getParameter("type").equals("legal"))
                customerType = false;
            Customer existingCustomer;
            existingCustomer = customerDAO.selectByCustomerId(customerId);
            request.setAttribute("customer", existingCustomer);
            request.setAttribute("customerType", customerType);
            request.setAttribute("jalaliBirthDate", request.getParameter("jalaliBirthDate"));
            RequestDispatcher dispatcher = request.getRequestDispatcher("edit-customer.jsp");
            dispatcher.forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
