package model.to;

import java.sql.Date;

public class Customer {
    private String customerId;
    private String customerCode;
    private boolean customerType;   // true -> Real Customer        false -> Legal Customer
    private String customerName;
    private String customerFamily;
    private String customerFather;
    private Date customerBirthDate;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public boolean getCustomerType() {
        return customerType;
    }

    public void setCustomerType(boolean customerType) {
        this.customerType = customerType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerFamily() {
        return customerFamily;
    }

    public void setCustomerFamily(String customerFamily) {
        this.customerFamily = customerFamily;
    }

    public String getCustomerFather() {
        return customerFather;
    }

    public void setCustomerFather(String customerFather) {
        this.customerFather = customerFather;
    }

    public Date getCustomerBirthDate() {
        return customerBirthDate;
    }

    public void setCustomerBirthDate(Date customerBirthDate) {
        this.customerBirthDate = customerBirthDate;
    }
}
