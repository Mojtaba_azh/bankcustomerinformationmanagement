package model.dao;

import model.to.Customer;
import utility.BankDatabaseUtility;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO {

    public boolean insert(Customer customer) throws SQLException {
        Connection connection = BankDatabaseUtility.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO bank.customer (customer_code, customer_type, customer_name, customer_family, customer_father, customer_birthdate) VALUES (?,?,N?,N?,N?,?);");
        preparedStatement.setString(1, customer.getCustomerCode());
        preparedStatement.setBoolean(2, customer.getCustomerType());
        preparedStatement.setString(3, customer.getCustomerName());
        preparedStatement.setString(4, customer.getCustomerFamily());
        preparedStatement.setString(5, customer.getCustomerFather());
        preparedStatement.setDate(6, customer.getCustomerBirthDate());
        boolean rowInserted = preparedStatement.executeUpdate() > 0;
        return rowInserted;
    }

    public Customer selectByCustomerId(String customerId) throws SQLException {
        Connection connection = BankDatabaseUtility.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM bank.customer WHERE customer_id = ?;");
        preparedStatement.setLong(1, Long.parseLong(customerId));
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        Customer customer = new Customer();
        customer.setCustomerId(String.valueOf(resultSet.getLong("customer_id")));
        customer.setCustomerCode(resultSet.getString("customer_code"));
        customer.setCustomerType(resultSet.getBoolean("customer_type"));
        customer.setCustomerName(resultSet.getString("customer_name"));
        customer.setCustomerFamily(resultSet.getString("customer_family"));
        customer.setCustomerFather(resultSet.getString("customer_father"));
        customer.setCustomerBirthDate(resultSet.getDate("customer_birthdate"));
        return customer;
    }

    public String selectLastCustomerId() throws SQLException {
        Connection connection = BankDatabaseUtility.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT max(customer_id) AS last_id FROM bank.customer;");
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        String customerId = String.valueOf(resultSet.getLong("last_id"));
        return customerId;
    }

    public boolean delete(String customerId) throws SQLException {
        Connection connection = BankDatabaseUtility.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM bank.customer WHERE customer_id = ?;");
        preparedStatement.setLong(1, Long.parseLong(customerId));
        boolean rowDeleted = preparedStatement.executeUpdate() > 0;
        return rowDeleted;
    }

    public boolean update(Customer customer) throws SQLException {
        Connection connection = BankDatabaseUtility.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("UPDATE bank.customer SET customer_code=?, customer_name=?, customer_family=?, customer_father=?, customer_birthdate=? where customer_id = ?;");
        preparedStatement.setString(1, customer.getCustomerCode());
        preparedStatement.setString(2, customer.getCustomerName());
        preparedStatement.setString(3, customer.getCustomerFamily());
        preparedStatement.setString(4, customer.getCustomerFather());
        preparedStatement.setDate(5, customer.getCustomerBirthDate());
        preparedStatement.setLong(6, Long.parseLong(customer.getCustomerId()));
        boolean rowUpdated = preparedStatement.executeUpdate() > 0;
        return rowUpdated;
    }

    public List<Customer> selectByParameters(Boolean customerType, String customerId, String customerCode, String customerName, String customerFamily, String birthDateFrom, String birthDateTo) throws SQLException {
        Connection connection = BankDatabaseUtility.getConnection();
        ResultSet resultSet = null;
        if (birthDateFrom.equals("") || birthDateFrom == null)
            birthDateFrom = "0000-00-00";
        if (birthDateTo.equals("") || birthDateTo == null)
            birthDateTo = "9999-00-00";
        if (customerId.equals("") || customerId == null)
            if (customerCode.equals("") || customerCode == null) {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM bank.customer WHERE customer_type=? AND customer_name LIKE ? AND customer_family LIKE ? AND customer_birthdate BETWEEN ? AND ?;");
                preparedStatement.setBoolean(1, customerType);
                preparedStatement.setString(2, customerName + "%");
                preparedStatement.setString(3, customerFamily + "%");
                preparedStatement.setString(4, birthDateFrom);
                preparedStatement.setString(5, birthDateTo);
                resultSet = preparedStatement.executeQuery();
            } else {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM bank.customer WHERE customer_type=? AND customer_code = ?;");
                preparedStatement.setBoolean(1, customerType);
                preparedStatement.setString(2, customerCode);
                resultSet = preparedStatement.executeQuery();
            }
        else {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM bank.customer WHERE customer_type=? AND customer_id = ?;");
            preparedStatement.setBoolean(1, customerType);
            preparedStatement.setLong(2, Long.parseLong(customerId));
            resultSet = preparedStatement.executeQuery();
        }
        ArrayList<Customer> customers = new ArrayList<>();
        while (resultSet.next()) {
            Customer customer = new Customer();
            customer.setCustomerId(String.valueOf(resultSet.getLong("customer_id")));
            customer.setCustomerCode(resultSet.getString("customer_code"));
            customer.setCustomerType(resultSet.getBoolean("customer_type"));
            customer.setCustomerName(resultSet.getString("customer_name"));
            customer.setCustomerFamily(resultSet.getString("customer_family"));
            customer.setCustomerFather(resultSet.getString("customer_father"));
            customer.setCustomerBirthDate(resultSet.getDate("customer_birthdate"));
            customers.add(customer);
        }
        return customers;
    }
}
